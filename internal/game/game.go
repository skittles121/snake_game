package game

import (
	"io/fs"
	"path/filepath"
	"snake/internal/board"
	"snake/internal/render"
	"snake/internal/snake"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	screenWidth  = 1280
	screenHeight = 720
)

const (
	GraphicsFolder = "Graphics"
	FoodImg        = "apple.png"
)

const (
	cellSize = 40
	rows     = screenHeight / 40
	col      = screenWidth / 40
)

type Game struct {
	board *board.Board
}

func (g *Game) Update() error {
	return g.board.Update()
}

func (g *Game) Draw(screen *ebiten.Image) {
	g.board.Draw(screen)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return screenWidth, screenHeight
}

func readGraphicsDir() []string {
	var assestsList []string
	filepath.Walk(GraphicsFolder, func(path string, info fs.FileInfo, err error) error {
		if info.Name() == GraphicsFolder {
			return nil
		}
		assestsList = append(assestsList, info.Name())
		return nil
	})

	return assestsList
}

func NewGame() *Game {
	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle("Hello, World!")
	// initate assestLoader
	assestLoader := render.NewAssestLoader(GraphicsFolder, readGraphicsDir())
	// set snake coordinates
	coordinates := &snake.Coordinates{X: col / 2 * cellSize, Y: rows / 2 * cellSize}
	// create snakeBoadyParts
	snakeBodyParts := snake.NewBodyParts(assestLoader.LoadAssests().SnakeGameImgs, coordinates, cellSize)
	// create new snake
	snakeBody := snake.NewSnake(snakeBodyParts, cellSize, assestLoader.LoadAssests().SnakeGameImgs)
	// init the game
	return &Game{
		board: board.NewBoard(cellSize, rows, col, snakeBody, assestLoader.LoadAssests().SnakeGameImgs[FoodImg]),
	}
}
