package board

import (
	"errors"
	"fmt"
	"math/rand"
	"snake/internal/snake"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type Board struct {
	hasDrawn   bool
	cellSize   int
	rows       int
	col        int
	food       *snake.Fruit
	points     int
	snakeBody  *snake.SnakeBody
	fruitImg   *ebiten.Image
	isgameOver bool
}

// NewBoard takes cellSize , rows, col snakeBody and fruitImg
func NewBoard(cellSize int, rows int, col int, snakeBody *snake.SnakeBody, fruitImg *ebiten.Image) *Board {
	snakeBody.SetSnakeTicker(time.NewTicker(time.Millisecond * 150))
	board := &Board{
		cellSize:  cellSize,
		rows:      rows,
		col:       col,
		snakeBody: snakeBody,
		fruitImg:  fruitImg,
	}
	board.placeFood()
	return board
}

func (b *Board) dir() (ebiten.Key, bool) {
	if inpututil.IsKeyJustPressed(ebiten.KeyArrowUp) {
		return ebiten.KeyArrowUp, true
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyArrowLeft) {
		return ebiten.KeyArrowLeft, true
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyArrowRight) {
		return ebiten.KeyArrowRight, true
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyArrowDown) {
		return ebiten.KeyArrowDown, true
	}

	return 0, false
}

func (b *Board) IsSnakeOutOfBound() bool {
	head := b.snakeBody.SnakeHead()

	return head.Pos.X < 0 || head.Pos.Y < 0 || head.Pos.X > float64(b.col*b.cellSize) || head.Pos.Y > float64(b.rows*b.cellSize)
}

func (b *Board) Update() error {
	fmt.Println(ebiten.ActualFPS())
	if b.snakeBody.SnakeHeadHitBody() || b.IsSnakeOutOfBound() {
		b.isgameOver = true
		return errors.New(fmt.Sprintf("Game over you died. Points: %d ", b.points))
	}
	if !b.isgameOver && b.hasDrawn {
		if key, ok := b.dir(); ok {
			b.snakeBody.SnakeDirection(key)
		}
		b.snakeBody.Move()
		if b.snakeBody.SnakeHeadHit(b.food.Coordinates()) {
			b.points++
			b.placeFood()
			b.snakeBody.GrowBody()
		}
		if b.points/5 > 1 && b.points < 5 {
			b.snakeBody.SetSnakeTicker(time.NewTicker(time.Microsecond * time.Duration(150/(b.points/5))))
		}
	}
	return nil
}

func (b *Board) Draw(screen *ebiten.Image) {
	b.hasDrawn = false
	op := &ebiten.DrawImageOptions{}
	for i := 0; i < len(b.snakeBody.SnakeBodyParts); i++ {
		op.GeoM.Translate(b.snakeBody.SnakeBodyParts[i].Pos.X, b.snakeBody.SnakeBodyParts[i].Pos.Y)
		screen.DrawImage(b.snakeBody.SnakeBodyParts[i].Body, op)
		op.GeoM.Reset()
	}

	if b.food != nil {
		op.GeoM.Reset()
		op.GeoM.Translate(b.food.Coordinates().X, b.food.Coordinates().Y)
		screen.DrawImage(b.food.FuritImg(), op)
	}

	b.hasDrawn = true
}

func (b *Board) placeFood() {
	coordinates := &snake.Coordinates{}
	rand.New(rand.NewSource(time.Now().Unix()))
	for {
		coordinates.X = float64(rand.Intn(b.col)) * float64(b.cellSize)
		coordinates.Y = float64(rand.Intn(b.rows)) * float64(b.cellSize)
		if !b.snakeBody.SnakeHeadHit(coordinates) {
			break
		}
	}

	b.food = snake.NewFruit(coordinates, b.fruitImg)
}
