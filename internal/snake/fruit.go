package snake

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type Fruit struct {
	fruiteImg   *ebiten.Image
	coordinates *Coordinates
}

func NewFruit(coordinates *Coordinates, fruiteImg *ebiten.Image) *Fruit {
	return &Fruit{
		fruiteImg:   fruiteImg,
		coordinates: coordinates,
	}
}

func (f *Fruit) Coordinates() *Coordinates {
	return f.coordinates
}

func (f *Fruit) FuritImg() *ebiten.Image {
	return f.fruiteImg
}
