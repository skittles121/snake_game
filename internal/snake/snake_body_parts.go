package snake

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type Coordinates struct {
	X float64
	Y float64
}
type BodyPart struct {
	Body      *ebiten.Image
	Pos       *Coordinates
	Direction ebiten.Key
}

func (s *BodyPart) updatePos(cellSize float64, snakeImgs map[string]*ebiten.Image) {
	switch s.Direction {
	case ebiten.KeyUp:
		s.Pos.Y -= cellSize
		s.Body = snakeImgs["head_up.png"]
	case ebiten.KeyDown:
		s.Pos.Y += cellSize
		s.Body = snakeImgs["head_down.png"]
	case ebiten.KeyLeft:
		s.Pos.X -= cellSize
		s.Body = snakeImgs["head_left.png"]
	case ebiten.KeyRight:
		s.Pos.X += cellSize
		s.Body = snakeImgs["head_right.png"]
	}
}

func (s *BodyPart) updateBody(snakeImgs map[string]*ebiten.Image, prv *BodyPart, next *BodyPart) {
	s.Pos.X = prv.Pos.X
	s.Pos.Y = prv.Pos.Y
	s.Direction = prv.Direction
	if next.Direction == ebiten.KeyUp && prv.Direction == ebiten.KeyUp || next.Direction == ebiten.KeyDown && prv.Direction == ebiten.KeyDown {
		s.Body = snakeImgs["body_vertical.png"]
	} else if next.Direction == ebiten.KeyRight && prv.Direction == ebiten.KeyRight || next.Direction == ebiten.KeyLeft && prv.Direction == ebiten.KeyLeft {
		s.Body = snakeImgs["body_horizontal.png"]
	} else {
		if (next.Direction == ebiten.KeyRight && prv.Direction == ebiten.KeyDown) || (next.Direction == ebiten.KeyUp && prv.Direction == ebiten.KeyLeft) {
			s.Body = snakeImgs["body_bottomleft.png"]
		} else if (next.Direction == ebiten.KeyLeft && prv.Direction == ebiten.KeyDown) || (next.Direction == ebiten.KeyUp && prv.Direction == ebiten.KeyRight) {
			s.Body = snakeImgs["body_bottomright.png"]
		} else if (next.Direction == ebiten.KeyRight && prv.Direction == ebiten.KeyUp) || (next.Direction == ebiten.KeyDown && prv.Direction == ebiten.KeyLeft) {
			s.Body = snakeImgs["body_topleft.png"]
		} else if (next.Direction == ebiten.KeyLeft && prv.Direction == ebiten.KeyUp) || (next.Direction == ebiten.KeyDown && prv.Direction == ebiten.KeyRight) {
			s.Body = snakeImgs["body_topright.png"]
		}
	}
}

func (s *BodyPart) updateTail(snakeImgs map[string]*ebiten.Image, prv *BodyPart) {
	s.Pos.X = prv.Pos.X
	s.Pos.Y = prv.Pos.Y
	s.Direction = prv.Direction
	switch prv.Direction {
	case ebiten.KeyUp:
		s.Body = snakeImgs["tail_down.png"]
	case ebiten.KeyDown:
		s.Body = snakeImgs["tail_up.png"]
	case ebiten.KeyLeft:
		s.Body = snakeImgs["tail_right.png"]
	case ebiten.KeyRight:
		s.Body = snakeImgs["tail_left.png"]
	}
}

func NewBodyParts(snakeGameImgs map[string]*ebiten.Image, coordOfHead *Coordinates, cellSize int) []*BodyPart {
	snakeBodyParts := make([]*BodyPart, 0, 10)
	snakeHead := &BodyPart{
		Body:      snakeGameImgs["head_up.png"],
		Pos:       coordOfHead,
		Direction: ebiten.KeyUp,
	}
	snakeBody := &BodyPart{
		Body: snakeGameImgs["body_vertical.png"],
	}
	snakeTail := &BodyPart{
		Body: snakeGameImgs["tail_down.png"],
	}
	snakeBodyParts = append(snakeBodyParts, snakeHead)
	snakeBodyParts = append(snakeBodyParts, snakeBody)
	snakeBodyParts = append(snakeBodyParts, snakeTail)

	for i := 1; i < len(snakeBodyParts); i++ {
		snakeBodyParts[i].Direction = snakeBodyParts[i-1].Direction
		snakeBodyParts[i].Pos = &Coordinates{}
		snakeBodyParts[i].Pos.X = snakeBodyParts[i-1].Pos.X
		snakeBodyParts[i].Pos.Y = snakeBodyParts[i-1].Pos.Y + float64(cellSize)
	}

	return snakeBodyParts
}
