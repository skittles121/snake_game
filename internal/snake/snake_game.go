package snake

import (
	"time"

	"github.com/hajimehoshi/ebiten/v2"
)

type Direction int

type SnakeBody struct {
	SnakeBodyParts []*BodyPart
	cellSize       int
	snakeImgs      map[string]*ebiten.Image
	MovmentTimer   *time.Ticker
}

func (s *SnakeBody) UpdateDirection(direction ebiten.Key) {
	s.SnakeBodyParts[0].Direction = direction
}

func NewSnake(SnakeBodyParts []*BodyPart, CellSize int, SnakeImgs map[string]*ebiten.Image) *SnakeBody {
	return &SnakeBody{
		SnakeBodyParts: SnakeBodyParts,
		cellSize:       CellSize,
		snakeImgs:      SnakeImgs,
	}
}

func (s *SnakeBody) SnakeDirection(newDir ebiten.Key) {
	opposites := map[ebiten.Key]ebiten.Key{
		ebiten.KeyArrowUp:    ebiten.KeyArrowDown,
		ebiten.KeyArrowRight: ebiten.KeyArrowLeft,
		ebiten.KeyArrowDown:  ebiten.KeyArrowUp,
		ebiten.KeyArrowLeft:  ebiten.KeyArrowRight,
	}

	if o, ok := opposites[newDir]; ok && o != s.SnakeHead().Direction {
		s.SnakeHead().Direction = newDir
	}
}

func (s *SnakeBody) SnakeHeadHit(coordinates *Coordinates) bool {
	bl := int(s.SnakeHead().Pos.X) == int(coordinates.X) && int(s.SnakeHead().Pos.Y) == int(coordinates.Y)
	return bl
}

func (s *SnakeBody) SnakeHeadHitBody() bool {
	for i := 1; i < len(s.SnakeBodyParts); i++ {
		if s.SnakeHeadHit(s.SnakeBodyParts[i].Pos) {
			return true
		}
	}
	return false
}

func (s *SnakeBody) IsSnakeOutofBound() bool {
	head := s.SnakeHead()

	return (head.Pos.X <= 0 && head.Pos.Y <= 0)
}

func (s *SnakeBody) SnakeHead() *BodyPart {
	return s.SnakeBodyParts[0]
}

func (s *SnakeBody) GrowBody() {
	newTail := &BodyPart{}
	newTail.Pos = &Coordinates{}
	newTail.Pos.X = s.SnakeBodyParts[len(s.SnakeBodyParts)-1].Pos.X
	newTail.Pos.Y = s.SnakeBodyParts[len(s.SnakeBodyParts)-1].Pos.Y
	newTail.Body = s.SnakeBodyParts[len(s.SnakeBodyParts)-1].Body
	s.SnakeBodyParts = append(s.SnakeBodyParts, newTail)
	s.SnakeBodyParts[len(s.SnakeBodyParts)-1].Body = s.SnakeBodyParts[len(s.SnakeBodyParts)-2].Body
}

func (s *SnakeBody) SetSnakeTicker(ticker *time.Ticker) {
	s.MovmentTimer = ticker
}

func (s *SnakeBody) Move() {
	select {
	case <-s.MovmentTimer.C:
		for i := len(s.SnakeBodyParts) - 1; i >= 0; i-- {
			if i == 0 {
				s.SnakeBodyParts[i].updatePos(float64(s.cellSize), s.snakeImgs)
			} else if i == len(s.SnakeBodyParts)-1 {
				s.SnakeBodyParts[i].updateTail(s.snakeImgs, s.SnakeBodyParts[i-1])
			} else {
				s.SnakeBodyParts[i].updateBody(s.snakeImgs, s.SnakeBodyParts[i-1], s.SnakeBodyParts[i+1])
			}
		}
	default:

	}
}
