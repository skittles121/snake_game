package render

import (
	_ "image/png"
	"log"
	"path/filepath"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

type SnakeGameAssests struct {
	SnakeGameImgs map[string]*ebiten.Image
}

type AssestLoader interface {
	LoadAssests() *SnakeGameAssests
}

func NewAssestLoader(assestPath string, assestNames []string) AssestLoader {
	return &assestLoader{assestPath: assestPath, assestNames: removeDuplicateInString(assestNames)}
}

func removeDuplicateInString(slice []string) []string {
	result := make([]string, 0, len(slice))
	mp := make(map[string]struct{})
	for _, value := range slice {
		if _, ok := mp[value]; !ok {
			mp[value] = struct{}{}
			result = append(result, value)
		}
	}
	return result
}

type assestLoader struct {
	assestPath  string
	assestNames []string
}

func (a *assestLoader) LoadAssests() *SnakeGameAssests {
	gameAssest := &SnakeGameAssests{SnakeGameImgs: make(map[string]*ebiten.Image)}
	path, err := filepath.Abs(a.assestPath)
	path += "/"
	if err != nil {
		log.Fatal("ERROR while abs path:", err)
	}
	for i := range a.assestNames {
		img, _, err := ebitenutil.NewImageFromFile(path + a.assestNames[i])
		if err != nil {
			log.Fatal("Error unable to read img:", err)
		}

		gameAssest.SnakeGameImgs[a.assestNames[i]] = img
	}
	return gameAssest
}
