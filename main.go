package main

import (
	"log"
	"net/http"
	"snake/internal/game"

	_ "net/http/pprof"

	"github.com/hajimehoshi/ebiten/v2"
)

func main() {
	go func() {
		http.ListenAndServe("localhost:6060", nil)
	}()
	if err := ebiten.RunGame(game.NewGame()); err != nil {
		log.Fatal(err)
	}
}
